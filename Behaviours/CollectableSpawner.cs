﻿using System.Collections;
using UnityEngine;
using UnityAtoms;
using System.Collections.Generic;

namespace MutCommon.UnityAtoms
{
    public class CollectableSpawner : MonoBehaviour
    {
        [Header("Parameters")]
        public FloatReference CooldownTime;

        public GameObjectReference ThingToSpawn;
        public GameObjectReference SecondarySpawn;

        [Header("Placement")]
        public bool DontFixHeight = false;
        public float DistanceFromGround = 0.5f;
        public float UpColliderBuffer = 0.5f;
        public LayerMask collisionLayer;

        [Header("State Variables")]
        // TODO: Se um dia implementarem isso, dá pra fazer o tempo restante e a bool serem atoms e pra usar em UI: https://github.com/AdamRamberg/unity-atoms/issues/53
        [SerializeField]
        private float cooldown;

        public float Cooldown => cooldown;

        [SerializeField]
        private bool isInCooldown = false;

        public bool IsInCooldown => isInCooldown;

        private GameObject primarySpawned;
        private GameObject secondarySpawned;

        private void Start()
        {
            CreateThing();
        }

        private bool PositionRaycast(out RaycastHit hitInfo)
          => Physics.Raycast(transform.position + Vector3.up * UpColliderBuffer, Vector3.down, out hitInfo, DistanceFromGround + UpColliderBuffer, collisionLayer);

        private Vector3 GetSpawnPosition()
        {
            if (PositionRaycast(out var hitInfo))
            {
                return hitInfo.point + DistanceFromGround * Vector3.up;
            }

            return transform.position;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            if (PositionRaycast(out var hitInfo))
            {
                Gizmos.DrawLine(transform.position, hitInfo.point);
            }

            foreach (var meshFilter in ThingToSpawn.Value.GetComponentsInChildren<MeshFilter>())
            {
                Gizmos.DrawMesh(meshFilter.sharedMesh, GetSpawnPosition(), Quaternion.identity);
            }
        }

        // TODO: add condition for when it can be respawned (eg: dota's pull)
        //public bool CanSpawn;

        void CreateThing()
        {
            primarySpawned = CreateThing(ThingToSpawn.Value);
            if (SecondarySpawn?.Value != null)
            {
                secondarySpawned = CreateThing(SecondarySpawn.Value);
                secondarySpawned.SetActive(false);
            }
        }

        public void ToggleSpawned()
        {
            if (secondarySpawned != null)
            {
                if (primarySpawned != null)
                    primarySpawned.SetActive(false);
                secondarySpawned?.SetActive(true);
            }
        }

        public GameObject CreateThing(GameObject thingToSpawn)
        {
            var thing = Instantiate(thingToSpawn, GetSpawnPosition(), Quaternion.identity, this.transform);
            thing.SetActive(true);

            var triggerEvent = thing.GetComponent<OnDestroyUnityEvent>() ?? thing.AddComponent<OnDestroyUnityEvent>();
            if (triggerEvent.callback == null)
            {
                triggerEvent.callback = new UnityEngine.Events.UnityEvent();
            }

            triggerEvent.callback.AddListener(() =>
            {
                if (!this.isActiveAndEnabled) return;
                this.DoNextFrame(() =>
          {
              if (!this.isActiveAndEnabled) return;
              Respawn();
          });
            });
            return thing;
        }

        private IEnumerator RespawnCoroutine()
        {
            isInCooldown = true;
            yield return CoroutineHelpers.InterpolateByTime(CooldownTime.Value, (k) =>
            {
                cooldown = 1 - k;
            });

            CreateThing();

            isInCooldown = false;
        }

        [ContextMenu("Respawn")]
        public void Respawn()
        {
            if (isInCooldown)
            {
                UberDebug.LogWarningChannel("MutCommon.unityatoms", "Tried to call respwan on a respawner on cooldown");
                return;
            };

            StartCoroutine(RespawnCoroutine());
        }
    }
}