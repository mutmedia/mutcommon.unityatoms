﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms;
using System.Linq;
using System;

namespace MutCommon.UnityAtoms
{

  public class AtomStringInterpolator : MonoBehaviour
  {
    public StringReference interpolation;

    // SUPER HACK
    public List<UnityEngine.Object> variableObjects;
    private IEnumerable<AtomVariable<object, AtomEvent<object>, AtomEvent<object, object>>> variables;

    public StringVariable output;

    private void OnValidate()
    {
      foreach (var v in variableObjects)
      {
        if (!AreSameGenericSubclass(typeof(FloatVariable), v.GetType()))
        {
          UberDebug.LogErrorChannel("MutCommon.UnityAtoms", $"{variableObjects.IndexOf(v)} is not a valid atom");
        }
      }
    }

    void Awake()
    {
      foreach (var v in variableObjects)
      {

        if (v is FloatVariable)
        {
          var castv = v as FloatVariable;
          if (castv.Changed == null)
          {
            castv.Changed = ScriptableObject.CreateInstance<FloatEvent>();
          }
          castv.Changed.Register(UpdateString);
          break;
        }
        else if (v is IntVariable)
        {
          var castv = v as IntVariable;
          if (castv.Changed == null)
          {
            castv.Changed = ScriptableObject.CreateInstance<IntEvent>();
          }
          castv.Changed.Register(UpdateString);
          break;
        }
        else if (v is StringVariable)
        {
          var castv = v as StringVariable;
          if (castv.Changed == null)
          {
            castv.Changed = ScriptableObject.CreateInstance<StringEvent>();
          }
          castv.Changed.Register(UpdateString);
          break;
        }
        else
        {
          UberDebug.LogErrorChannel("MutCommon.UnityAtoms", $"{variableObjects.IndexOf(v)} is not an atom that can be listened to");
          break;
        }
      }

      UpdateString();
    }

    void UpdateString()
    {
      output.Value = string.Format(interpolation.Value, variableObjects.Select(v => (v as AtomBaseVariable).BaseValue ?? "null").ToArray());
    }

    static bool AreSameGenericSubclass(Type a, Type b)
    {
      while (a != null && a != typeof(object))
      {
        var curA = a.IsGenericType ? a.GetGenericTypeDefinition() : a;

        while (b != null && b != typeof(object))
        {
          var curB = b.IsGenericType ? b.GetGenericTypeDefinition() : b;
          if (curA == curB)
          {
            return true;
          }
          b = b.BaseType;
        }

        a = a.BaseType;
      }

      return false;
    }
  }
}