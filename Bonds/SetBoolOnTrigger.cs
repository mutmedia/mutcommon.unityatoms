﻿using UnityAtoms;

namespace MutCommon.UnityAtoms
{
  public class SetBoolOnTrigger : SetVariableOnTrigger<BoolReference, BoolVariable, BoolConstant, bool>
  {
  }
}
