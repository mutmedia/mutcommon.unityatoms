﻿using UnityAtoms;

namespace MutCommon.UnityAtoms
{
  public class SetIntOnTrigger : SetVariableOnTrigger<IntReference, IntVariable, IntConstant, int>
  {
  }
}
