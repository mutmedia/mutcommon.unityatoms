﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms;

namespace MutCommon.UnityAtoms
{
  public class RaiseEventWithValueBool : RaiseEventWithValue<BoolEvent, BoolReference, BoolVariable, BoolConstant, bool>
  {
  }
}