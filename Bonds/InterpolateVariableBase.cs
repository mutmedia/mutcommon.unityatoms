﻿using UnityAtoms;
using UnityEngine;

namespace MutCommon.UnityAtoms
{
  public abstract class InterpolateVariableBase<I, R, V, C, T> : MonoBehaviour
    where V : AtomBaseVariable<T>
    where C : AtomBaseVariable<T>
    where R : AtomReference<T, V, C>
    where I : IInterpolator<T>, new()
  {
    [SerializeField]
    private FloatReference t;

    [SerializeField]
    private R Value0;

    [SerializeField]
    private R Value1;

    [SerializeField]
    private V Output;

    private readonly I interpolator = new I();

    void Update() => Output.Value = interpolator.Interpolate(Value0.Value, Value1.Value, t.Value);
  }
}