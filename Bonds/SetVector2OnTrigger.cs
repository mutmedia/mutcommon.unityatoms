﻿using UnityAtoms;
using UnityEngine;

namespace MutCommon.UnityAtoms
{
  public class SetVector2OnTrigger : SetVariableOnTrigger<Vector2Reference, Vector2Variable, Vector2Constant, Vector2>
  {
  }
}