﻿using UnityAtoms;
using UnityEngine;

namespace MutCommon.UnityAtoms
{
  public abstract class OscilateVariableBase<I, R, V, C, T> : MonoBehaviour
    where V : AtomBaseVariable<T>
    where C : AtomBaseVariable<T>
    where R : AtomReference<T, V, C>
    where I : IInterpolator<T>, new()
  {
    [SerializeField]
    private V value;

    [SerializeField]
    private FloatReference frequency;

    [SerializeField]
    private FloatReference phase;

    [SerializeField]
    private R maxValue;

    [SerializeField]
    private R minValue;

    private readonly I interpolator = new I();

    void Awake()
    {
      f0 = frequency.Value;
      t0 = Time.time;
      tt = t0;
      p = 0;
    }

    float f0;
    float t0;
    float tt;
    float p;

    void Update()
    {
      var t = Time.time - t0;
      var f = frequency.Value;
      p += tt * (f - f0);
      var x = Mathf.Sin(Mathf.PI * 2 * (f * t - p) + phase * Mathf.Deg2Rad);
      f0 = f;
      tt = t;

      value.Value = interpolator.Interpolate(minValue.Value, maxValue.Value, 0.5f * x + 0.5f);
    }
  }
}
