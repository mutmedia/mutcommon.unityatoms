﻿using UnityAtoms;
using UnityEngine;

namespace MutCommon.UnityAtoms
{
  public class SetVector3OnTrigger : SetVariableOnTrigger<Vector3Reference, Vector3Variable, Vector3Constant, Vector3>
  {
  }
}