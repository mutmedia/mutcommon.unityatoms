﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms;

namespace MutCommon.UnityAtoms
{
  public class RaiseEventWithValue<E, R, V, C, T> : MonoBehaviour
    where E : AtomEvent<T>
    where R : AtomReference<T, V, C>
    where V : AtomBaseVariable<T>
    where C : AtomBaseVariable<T>
  {
    [SerializeField] private E eventToRaise;
    [SerializeField] private R value;

    public void Raise()
      => eventToRaise.Raise(value.Value);

    public void Raise(V variable)
      => eventToRaise.Raise(variable.Value);

    public void Raise(C constant)
      => eventToRaise.Raise(constant.Value);
  }
}