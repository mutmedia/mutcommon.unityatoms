﻿using UnityEngine;
using UnityAtoms;

namespace MutCommon.UnityAtoms
{
  public class FromRange : MonoBehaviour
  {
    public FloatReference Min;
    public FloatReference Max;
    public FloatVariable In;
    public FloatVariable Out;

    void Update()
    {
      if (In != null)
      {
        Out?.SetValue(Mathf.InverseLerp(Min, Max, In.Value));
      }
    }
  }
}