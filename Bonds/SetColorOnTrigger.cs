﻿using UnityAtoms;
using UnityEngine;

namespace MutCommon.UnityAtoms
{
  public class SetColorOnTrigger : SetVariableOnTrigger<ColorReference, ColorVariable, ColorConstant, Color>
  {
  }
}
