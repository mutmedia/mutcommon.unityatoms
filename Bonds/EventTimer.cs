﻿using UnityEngine;
using UnityAtoms;
using System.Collections;
using System;

namespace MutCommon.UnityAtoms
{

    public class EventTimer : MonoBehaviour
    {
        [SerializeField]
        private FloatReference Timer;
        [SerializeField]
        private BoolReference IsPaused;
        [SerializeField]
        private VoidEvent Event;

        [SerializeField]
        private FloatVariable elapsed;

        public void Timeout(float duration) => StartCoroutine(DoTimeout(duration));
        public void Timeout(FloatConstant duration) => Timeout(duration.Value);
        public void Timeout() => Timeout(Timer.Value);

        IEnumerator DoTimeout(float duration) => DoTimeoutGetTime(duration, () => Time.deltaTime);

        IEnumerator DoTimeoutGetTime(float duration, Func<float> getDeltaTime)
        {
            float t = 0.0f;
            while (t < duration)
            {
                if (!IsPaused.Value)
                {
                    t += getDeltaTime();
                    if (elapsed != null)
                    {
                        elapsed.Value = t / duration;
                    }
                }
                yield return null;
            }
            Event.Raise();
        }

        public void TimeoutRealtime(float duration) => StartCoroutine(DoTimeoutRealtime(duration));
        public void TimeoutRealtime(FloatConstant duration) => TimeoutRealtime(duration.Value);
        public void TimeoutRealtime() => TimeoutRealtime(Timer.Value);

        IEnumerator DoTimeoutRealtime(float duration) => DoTimeoutGetTime(duration, () => Time.unscaledDeltaTime);

        public void TimeoutFrames(int frames) => StartCoroutine(DoTimeoutFrames(frames));
        public void TimeoutFrames(IntConstant frames) => TimeoutFrames(frames.Value);

        IEnumerator DoTimeoutFrames(int frames)
        {
            int i = 0;
            while (i < frames)
            {
                if (!IsPaused.Value)
                {
                    i++;
                    if (elapsed != null)
                    {
                        elapsed.Value = (float)i / (float)frames;
                    }
                }
                yield return null;
            }
            Event.Raise();
        }
    }
}