﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms;

namespace MutCommon.UnityAtoms
{
  public class RaiseEventWithValueFloat : RaiseEventWithValue<FloatEvent, FloatReference, FloatVariable, FloatConstant, float>
  {
  }
}