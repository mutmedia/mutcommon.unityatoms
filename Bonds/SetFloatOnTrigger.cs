﻿using UnityAtoms;

namespace MutCommon.UnityAtoms
{
  public class SetFloatOnTrigger : SetVariableOnTrigger<FloatReference, FloatVariable, FloatConstant, float>
  {
  }
}
