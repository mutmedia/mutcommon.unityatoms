﻿using UnityAtoms;
using UnityEngine;

namespace MutCommon.UnityAtoms
{
  public class SetTriggerBool : MonoBehaviour
  {
    [SerializeField]
    private LayerMask layers;

    [SerializeField]
    private BoolVariable output;
    private void OnTriggerEnter(Collider other)
    {
      if (((1 << other.gameObject.layer) & layers) != 0)
      {
        output.Value = true;
      }
    }

    private void OnTriggerExit(Collider other)
    {
      if (((1 << other.gameObject.layer) & layers) != 0)
      {
        output.Value = false;
      }
    }
  }
}