﻿using UnityAtoms;
using UnityEngine;

namespace MutCommon.UnityAtoms
{
  public class AXplusB : MonoBehaviour
  {
    public FloatVariable In;
    public FloatReference A;
    public FloatReference B;
    public FloatVariable Out;

    void Update()
    {
      if (In != null)
      {
        Out?.SetValue(A * In.Value + B);
      }
    }
  }
}