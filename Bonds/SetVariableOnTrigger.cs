﻿using UnityAtoms;
using UnityEngine;

namespace MutCommon.UnityAtoms
{
  public class SetVariableOnTrigger<R, V, C, T> : MonoBehaviour
    where V : AtomBaseVariable<T>
    where C : AtomBaseVariable<T>
    where R : AtomReference<T, V, C>
  {
    public LayerMask collisionLayers;

    public R Value;
    public V Output;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
      if (other.gameObject.tag == "Player")
      {
        Output.Value = Value.Value;
      }
    }
  }
}