﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms;

namespace MutCommon.UnityAtoms
{
  public class RaiseEventWithValueInt : RaiseEventWithValue<IntEvent, IntReference, IntVariable, IntConstant, int>
  {
  }
}